import os
import shlex
import subprocess
from time import sleep
from distutils.spawn import find_executable
from pype.env_modules import get_module_cmd


def submit(command, requirements, dependencies, log, profile):
    pype_exec = find_executable('pype')
    command = '%s --profile %s snippets --log %s %s' % (
        pype_exec, profile, log.__path__, command)
    log.log.info('Command: %s' % command)
    log.log.info('Requirements: %s' % requirements)
    log.log.info('Dependencies: %s' % dependencies)
    module = get_module_cmd()
    log.log.info('Load environment module torque')
    module('add', 'torque')
    stdout = os.path.join(log.__path__, 'stdout')
    stderr = os.path.join(log.__path__, 'stderr')
    log.log.info('Execution msub into working directory %s' % os.getcwd())
    log.log.info('Redirect stdin/stderr to folder %s' % log.__path__)
    command = '''#!/bin/bash
    exec 1>%s
    exec 2>%s
    exec %s ''' % (stdout, stderr, command)
    log.log.info('Retrive custom group environment variable')
    largs = []

    if len(dependencies) > 0:
        dependencies = ['afterany:%s' % dep for dep in dependencies]
        depend = ['-W', 'depend=%s' % ','.join(dependencies)]
        largs += depend
    if 'time' in requirements.keys():
        time = ['-l', 'walltime=%s' % requirements['time']]
        largs += time
    if 'mem' in requirements.keys():
        mem = ['-l', 'mem=%s' % requirements['mem']]
        largs += mem
    if 'ncpu' in requirements.keys():
        try:
            nodes = int(requirements['nodes'])
        except KeyError:
            nodes = 1
        cpus = ['-l', 'nodes=%i:ppn=%i' % (nodes, int(requirements['ncpu']))]
        largs += cpus
    msub_group = os.environ.get('PYPE_QUEUE_GROUP')
    if msub_group:
        log.log.info('Custom msub group set to %s' % msub_group)
        largs += ['-W', 'group_list=%s' % msub_group, '-A', msub_group]
    else:
        log.log.info('Custom msub group not set')
    echo = "echo '%s'" % command
    msub = ['qsub', '-V', '-o', '/dev/null', '-e',
            '/dev/null', '-d', os.getcwd()] + largs
    echo = shlex.split(echo)
    msub = shlex.split(' '.join(msub))
    log.log.info('Process command line with subprocess.Popen: %s | %s' %
                 (' '.join(echo), ' '.join(msub)))
    echo_proc = subprocess.Popen(echo, stdout=subprocess.PIPE)
    msub_proc = subprocess.Popen(
        msub, stdin=echo_proc.stdout, stdout=subprocess.PIPE)
    out = msub_proc.communicate()[0]
    log.log.info('Results job id: %s' % out.strip())
    sleep(1)
    return(out.strip())
