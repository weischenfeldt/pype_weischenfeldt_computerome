from pype.misc import generate_uid
from time import sleep
import shlex
import subprocess


def submit(command, requirements, dependencies, log, profile):
    command = 'python -m pype.commands --profile %s snippets --log %s %s' % (
        profile, log.__path__, command)
    log.log.info('Queue echo, command: %s' % command)
    log.log.info('Queue echo, requirements: %s' % requirements)
    log.log.info('Queue echo, dependencies: %s' % dependencies)
    echo = 'echo %s' % command
    echo = shlex.split(echo)
    echo_proc = subprocess.Popen(echo, stdout=subprocess.PIPE)
    out = echo_proc.communicate()[0]
    sleep(1)
    return(generate_uid(10)[-10:])
