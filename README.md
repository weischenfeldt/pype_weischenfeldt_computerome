# Bio_pype modules for the Weischenfeldt group tools in computerome

## installation

install the [`bio_pype`](https://bitbucket.org/ffavero/bio_pype) python module

Then clone the `weischenfeldt` repository:

```
pype repos install -f weischenfeldt
```

The modules works in the computerome HPC system, and rely on [environmental modules](http://modules.sourceforge.net/) loading.

In order to load all theneccessary modules, make sure to add the following line in the `~/.bashrc` profile:

```
module use /home/projects/cu_10027/apps/modulefiles
```

## Note

The modules provide support for running pipelines in the queuing system using the `qsub` and `msub` interfaces, as well parallel execution without relying on a particular scheduler (best for interactive session within a full node).

Some of the tools and resources are available to any computerome users, but many dependencies and programs are available only to members of the `cu_10027` group in computerome.

More details and usage are in the [Weischenfeldt group wiki](http://weischenfeldtlab.dk/wiki/doku.php?) page
