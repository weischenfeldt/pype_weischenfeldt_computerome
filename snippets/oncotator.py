import os
import shlex
import subprocess
from pype.misc import generate_uid
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 1, 'time': '48:00:00'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    return({'out': output})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name, help='Annotate various file format with Oncotator', add_help=False)


def oncotator_args(parser, subparsers, argv):
    parser.add_argument('-i', '--input', dest='input',
                        help='Input file', required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output file', required=True)
    parser.add_argument('--input_type', dest='itype',
                        help='File type for the input file, default VCF', default='VCF')
    parser.add_argument('--out_type', dest='otype',
                        help='File type for the output file, default VCF', default='VCF')
    return parser.parse_args(argv)


def oncotator(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = oncotator_args(add_parser(
        subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['intelperflibs']))
    module('add', program_string(profile.programs['gcc']))    
    module('add', program_string(profile.programs['R361']))   
    module('add', program_string(profile.programs['oncotator']))

    db = profile.files['oncotator_db']
    log.log.info('Use database folder %s' % db)
    build = profile.genome_build
    log.log.info('Use genome build %s' % build)

    log.log.info('Preparing oncotator command line')
    oncotator_cmd = ['oncotator', '-v', '--input_format', '%s' % args.itype, '--output_format',
                     '%s' % args.otype, '--db-dir', '%s' % db, args.input, args.out, build]
    oncotator_cmd = shlex.split(' '.join(map(str, oncotator_cmd)))

    log.log.info(' '.join(map(str, oncotator_cmd)))
    log.log.info('Execute oncotator with python subprocess.Popen')
    oncotator_proc = subprocess.Popen(oncotator_cmd)

    out0 = oncotator_proc.communicate()[0]
    log.log.info('Terminate oncotator')
