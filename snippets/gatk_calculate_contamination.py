import re
import os
import shlex
import subprocess
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 1, 'time': '48:00:00'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    return({'table': output})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(
        module_name, help='Calculate the fraction of reads coming from cross-sample contamination',
        add_help=False)


def gatk_calculate_contamination_args(parser, subparsers, argv):
    parser.add_argument('-i', '--input', dest='input',
                        help='Input pileup table', required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output table file', required=True)
    parser.add_argument('-m', '--matched', dest='matched',
                        help='Matched normal pileup table')
    return parser.parse_args(argv)


def gatk_calculate_contamination(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = gatk_calculate_contamination_args(add_parser(
        subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['java8']))
    module('add', program_string(profile.programs['gatk']))
    module('add', program_string(profile.programs['tabix']))
    gatk_dir = os.environ['GATK_DIR']
    # gatk_jar = os.path.join(gatk_dir, 'GenomeAnalysisTK.jar')
    # log.log.info('Use GATK jar file at %s' % gatk_jar)

    build = profile.genome_build
    log.log.info('Use genome build %s' % build)

    output = args.out


    log.log.info('Preparing gatk command line')
    gatk_cmd = ['gatk', 'CalculateContamination',
                '-I', args.input]
    if args.matched is None:
        gatk_cmd += ['-O', output]
    else:
        gatk_cmd += ['-matched', args.matched, '-O', output]

    gatk_cmd = shlex.split(' '.join(map(str, gatk_cmd)))

    log.log.info(' '.join(map(str, gatk_cmd)))
    log.log.info('Execute gatk with python subprocess.Popen')
    gatk_proc = subprocess.Popen(gatk_cmd)
    out2 = gatk_proc.communicate()[0]

    log.log.info('Terminate gatk')
