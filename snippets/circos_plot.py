import os
import shlex
import subprocess
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 1, 'time': '24:00:00', 'mem': '2gb'})


def results(argv):
    try:
        out_pdf = argv['--out']
    except KeyError:
        out_pdf = argv['-o']
    return({'pdf': out_pdf})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Circos plot of SV calls'),
                                 add_help=False)


def circos_plot_args(parser, subparsers, argv):
    parser.add_argument('-s', '--sv', dest='sv_file',
                        help='SV bedpe file', required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output pdf',
                        default=os.path.join(os.getcwd(), 'sv.pdf'))
    parser.add_argument('-t', '--title', dest='title',
                        help='Title description in the plot',
                        default='Circos plot')
    return parser.parse_args(argv)


def circos_plot(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = circos_plot_args(add_parser(
        subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['R3']))
    module('add', program_string(profile.programs['cov_plot']))
    r_script = os.environ['R_CIRCOS_PLOT']
    # Transform relative paths (if any) into full paths
    out_pdf = os.path.abspath(args.out)
    out_folder, file_name = os.path.split(out_pdf)

    if not os.path.exists(out_folder):
        log.log.info('Create output directory %s' % out_folder)
        os.makedirs(out_folder)

    log.log.info('Prepare circos_plot command line')
    echo_cmd = ("echo bedpe.file=\\'%s\\';"
                "header=\\'%s\\';"
                "out.file=\\'%s\\'") % (
        args.sv_file, args.title, out_pdf)

    Rscr_cmd = "cat - %s" % r_script
    Rproc_cmd = "R --vanilla --slave"
    echo_cmd = shlex.split(echo_cmd)
    Rscr_cmd = shlex.split(Rscr_cmd)
    Rproc_cmd = shlex.split(Rproc_cmd)
    log.log.info('%s | %s | %s' % (
        ' '.join(map(str, echo_cmd)),
        ' '.join(map(str, Rscr_cmd)),
        ' '.join(map(str, Rproc_cmd))))
    log.log.info('Execute circos_plot with python subprocess.Popen')
    echo_proc = subprocess.Popen(echo_cmd, stdout=subprocess.PIPE)
    Rscr_proc = subprocess.Popen(Rscr_cmd, stdin=echo_proc.stdout,
                                 stdout=subprocess.PIPE)
    Rproc_proc = subprocess.Popen(Rproc_cmd, stdin=Rscr_proc.stdout)
    echo_proc.stdout.close()
    Rscr_proc.stdout.close()
    out1 = Rproc_proc.communicate()[0]

    log.log.info('Terminate circos_plot')
