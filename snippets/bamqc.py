
import os
import shlex
import subprocess
import re
from pype.misc import generate_uid
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 1, 'time': '01:00:00'})


def results(argv):
    return None

def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Generate exome capture QC report with bamqc'),
                                 add_help=False)

                                 
def bamqc_args(parser, subparsers, argv):
    parser.add_argument('--bamfile', dest='bamfile',
                        help=('The bam file for analysis'),
                        required=True)
    parser.add_argument('--assembly', dest='assembly',
                        help=('The assembly version to use. Humans assemblies include: GRCh37, GRCh38, GRCh38_v90, NCBI34, NCBI35, and NCBI36.'),
                        required=True)  
    parser.add_argument('--outdir', dest='outdir',
                        help='Create all output files in the specified output directory',
                        required=True)
    parser.add_argument('--gff', dest='gff',
                        help=('Use the specified GFF file as annotation set'),
                        required=False)                        
    parser.add_argument('--species', dest='species',
                        help=('The genome species to use. If the couple species assembly does not exist, BamQC will try to download it'),
                        required=False, default='Homo sapiens')
    parser.add_argument('--available', action='store_true',
                        help='List the genomes available on the Babraham Server. Filter by supplying a partial match (accepts * and ?, and is case sensitive)',
                        required=False)
    parser.add_argument('--saved', action='store_true',
                        help='List the genomes that have already been downloaded',
                        required=False)
    parser.add_argument('--threads', dest='threads',
                        help='Specifies the number of files which can be processed simultaneously.',
                        required=False, default=1)
    parser.add_argument('--limits', dest='limits',
                        help='Specifies a non-default file which contains a set of criteria which will be used to determine the warn/error limits for the various modules',
                        required=False)
    parser.add_argument('--tempdir', dest='tempdir',
                        help='Selects a directory to be used for temporary files',
                        required=False, default='/home/projects/cu_10027/scratch/')
    return parser.parse_args(argv)

def bamqc(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = bamqc_args(add_parser(subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['bamqc']))

    log.log.info("Input BAM is: %s" % args.bamfile)
    log.log.info("bamqc output directory set to: %s" % args.outdir)
    
    genome = None
    defaultGenomeDir ='/home/projects/cu_10027/data/genomes/bamqc'
    exactGenomeDir = os.path.join(defaultGenomeDir, args.species, args.assembly)
    if os.path.exists(exactGenomeDir):
        genome =  exactGenomeDir


    outDir = args.outdir
    #Create outdir
    if not re.match('^/',outDir):
        outDir = os.path.abspath(outDir)
        log.log.info('Generated absolute path for outdir as: %s' % outDir)		
    if not os.path.exists(outDir):
        os.makedirs(outDir)
        log.log.info('Created outdir at: %s' % outDir) 
    
    bamqc_cmd = ['bamqc']    
    
    if args.available:
        bamqc_cmd.append("--available")
    elif args.saved:
        bamqc_cmd.append("--saved")
    else:
        if args.threads is not None:
            bamqc_cmd.append("--threads")
            bamqc_cmd.append(args.threads)
        
        if args.limits is not None:
            bamqc_cmd.append("--limits") 
            bamqc_cmd.append(args.limits)
        
        if args.tempdir is not None:
            bamqc_cmd.append("--dir")
            bamqc_cmd.append(args.tempdir)    
        
        if args.gff is not None:
            bamqc_cmd.append("--gff")
            bamqc_cmd.append(args.gff)  
        
        if genome is not None:               
            bamqc_cmd.append("--genome")
            bamqc_cmd.append(genome)
            log.log.info("Using genome location: %s" % genome)
        else:
            bamqc_cmd.append("--species")
            bamqc_cmd.append(args.species)
            bamqc_cmd.append("--assembly")
            bamqc_cmd.append(args.assembly)     
            log.log.info("Using species: %s" % args.species)
            log.log.info("Using assembly: %s" % args.assembly)
        
        bamqc_cmd.append("--outdir")
        bamqc_cmd.append(outDir)
        
        bamqc_cmd.append(args.bamfile)

    bamqc_cmd = map(str,bamqc_cmd)

    log.log.info('Exec Cmd is: %s' %  " ".join(bamqc_cmd))
    log.log.info('Execute bamqc with python subprocess.Popen')
    bamqc_proc = subprocess.Popen(bamqc_cmd, stdout=subprocess.PIPE)
    out0 = bamqc_proc.communicate()[0]
    log.log.info(out0)
    log.log.info('Finished bamqc')
