import os
import re
import shlex
import subprocess
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 1, 'time': '24:00:00', 'mem': '2gb'})


def results(argv):
    try:
        out_folder = argv['--out']
    except KeyError:
        out_folder = argv['-o']
    try:
        tumor_cov = argv['--tumor']
    except KeyError:
        tumor_cov = argv['-t']
    tumor_cov_path, tumor_sample_name = os.path.split(tumor_cov)
    tumor_sample_name = re.sub('.gz$', '', tumor_sample_name)
    tumor_sample_name = re.sub('.out$', '', tumor_sample_name)
    tumor_sample_name = re.sub('.cov$', '', tumor_sample_name)
    scaled_tumor_cov = os.path.join(out_folder,
                                    '%s.scaled.cov.gz' % tumor_sample_name)
    baf_path = os.path.join(out_folder,
                            '%s.baf.txt' % tumor_sample_name)

    return({'scaled_cov': scaled_tumor_cov})
    return({'bin_BAF_seqz': baf_path})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Coverage plot with SV calls'),
                                 add_help=False)


def cov_plot_args(parser, subparsers, argv):
    parser.add_argument('-n', '--normal', dest='normal',
                        help='Normal/control coverage file',
                        required=True, type=str)
    parser.add_argument('-t', '--tumor', dest='tumor',
                        help='Tumor coverage file',
                        required=True, type=str)
    parser.add_argument('-s', '--sv', dest='sv_file',
                        help='SV bedpe file', type=str)
    parser.add_argument('-z', '--seqz', dest='seqz_file',
                        help='Seqz file', type=str)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output folder', default=os.getcwd())
    return parser.parse_args(argv)


def cov_plot(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = cov_plot_args(add_parser(
        subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['intelperflibs']))
    module('add', program_string(profile.programs['gcc']))    
    module('add', program_string(profile.programs['R361']))        
    module('add', program_string(profile.programs['cov_plot']))
    r_script = os.environ['R_COV_PLOT']
    # Transform relative paths (if any) into full paths
    out_folder = os.path.abspath(args.out)
    normal_full_path = os.path.abspath(args.normal)
    tumor_full_path = os.path.abspath(args.tumor)
    seqz_file_path = os.path.abspath(args.seqz_file)

    normal_cov_path, normal_sample_name = os.path.split(normal_full_path)
    normal_sample_name = re.sub('.gz$', '', normal_sample_name)
    normal_sample_name = re.sub('.out$', '', normal_sample_name)
    normal_sample_name = re.sub('.cov$', '', normal_sample_name)
    normal_sample_name = re.sub('.gcnorm$', '', normal_sample_name)
    tumor_cov_path, tumor_sample_name = os.path.split(tumor_full_path)
    tumor_sample_name = re.sub('.gz$', '', tumor_sample_name)
    tumor_sample_name = re.sub('.out$', '', tumor_sample_name)
    tumor_sample_name = re.sub('.cov$', '', tumor_sample_name)
    scaled_tumor_cov = os.path.join(out_folder,
                                    '%s.scaled.cov.gz' % tumor_sample_name)
    tumor_sample_name = re.sub('.gcnorm$', '', tumor_sample_name)
    baf_path = os.path.join(out_folder,
                            '%s.baf.txt' % tumor_sample_name)

    if not os.path.exists(out_folder):
        log.log.info('Create output directory %s' % out_folder)
        os.makedirs(out_folder)
    os.chdir(out_folder)
    log.log.info('Prepare scale_cov command line')
    scale_cmd = ['scale_cov', '--reference', normal_full_path,
                 '--coverages', tumor_full_path, '--output', scaled_tumor_cov]
    scale_cmd = shlex.split(' '.join(map(str, scale_cmd)))
    log.log.info(' '.join(map(str, scale_cmd)))
    scale_proc = subprocess.Popen(scale_cmd)
    out0 = scale_proc.communicate()[0]
    if scale_proc.returncode != 0:
        msg = 'Error in command %s' % ' '.join(map(str, scale_cmd))
        log.log.error(msg)
        raise Exception(msg)

    log.log.info('Prepare bin_BAF_seqz command line')
    baf_cmd = ['bin_BAF_seqz.Rscipt', '--seqz_file', seqz_file_path ,
                 '--out_file', baf_path]
    baf_cmd = shlex.split(' '.join(map(str, baf_cmd)))
    log.log.info(' '.join(map(str, baf_cmd)))
    baf_proc = subprocess.Popen(baf_cmd)
    out0 = baf_proc.communicate()[0]
    if baf_proc.returncode != 0:
        msg = 'Error in command %s' % ' '.join(map(str, baf_cmd))
        log.log.error(msg)
        raise Exception(msg)

    log.log.info('Prepare cov_plot command line')

    if args.sv_file is None:
        echo_cmd = ("echo dataFile=c(\\'%s\\',\\'%s\\');"
                    "BAF=\\'%s\\';"
                    "sampleNames=c(\\'%s\\',\\'%s\\')") % (
            scaled_tumor_cov, normal_full_path, baf_path,
            tumor_sample_name, normal_sample_name)
    else:
        echo_cmd = ("echo dataFile=c(\\'%s\\',\\'%s\\');"
                    "svFile=\\'%s\\';"
                    "BAF=\\'%s\\';"
                    "sampleNames=c(\\'%s\\',\\'%s\\')") % (
            scaled_tumor_cov, normal_full_path, args.sv_file, baf_path,
            tumor_sample_name, normal_sample_name)

    Rscr_cmd = "cat - %s" % r_script
    Rproc_cmd = "R --vanilla --slave"
    echo_cmd = shlex.split(echo_cmd)
    Rscr_cmd = shlex.split(Rscr_cmd)
    Rproc_cmd = shlex.split(Rproc_cmd)
    log.log.info('%s | %s | %s' % (
        ' '.join(map(str, echo_cmd)),
        ' '.join(map(str, Rscr_cmd)),
        ' '.join(map(str, Rproc_cmd))))
    log.log.info('Execute cov-plot with python subprocess.Popen')
    echo_proc = subprocess.Popen(echo_cmd, stdout=subprocess.PIPE)
    Rscr_proc = subprocess.Popen(Rscr_cmd, stdin=echo_proc.stdout,
                                 stdout=subprocess.PIPE)
    Rproc_proc = subprocess.Popen(Rproc_cmd, stdin=Rscr_proc.stdout)
    echo_proc.stdout.close()
    Rscr_proc.stdout.close()
    out1 = Rproc_proc.communicate()[0]

    log.log.info('Terminate cov_plot')
