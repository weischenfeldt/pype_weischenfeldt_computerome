import os
import shlex
import subprocess
import gzip
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 1, 'time': '12:00:00'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    run_id = argv['--run-id']
    output = os.path.abspath(output)
    out_raw = os.path.join(output, run_id)
    out_filter = os.path.join(output, 'filter_calls', run_id)
    out_svfreq = '%s.svFreq' % out_filter
    out_pon = '%s.PoN' % out_svfreq
    out_svRescue = os.path.join(output, run_id)
    return({'raw_vcf': '%s.vcf.gz' % out_raw,
            'raw_tbi': '%s.vcf.gz.tbi' % out_raw,
            'raw_bedpe': '%s.bedpe.txt.gz' % out_raw,
            'germline_bedpe': '%s.germline.bedpe.txt' % out_pon,
            'germline_highConf_vcf':
                '%s.germline.highConf.vcf.gz' % out_pon,
            'germline_highConf_tbi':
                '%s.germline.highConf.vcf.gz.tbi' % out_pon,
            'germline_vcf': '%s.germline.vcf.gz' % out_pon,
            'germline_tbi': '%s.germline.vcf.gz.tbi' % out_pon,
            'germline_highConf_bedpe':
                '%s.germline.highConf.bedpe.txt' % out_pon,
            'svFreq_bedpe':
                '%s.bedpe.txt' % out_svfreq,
            'somatic_vcf': '%s.somatic.vcf.gz' % out_pon,
            'somatic_vcf_tbi': '%s.somatic.vcf.gz.tbi' % out_pon,
            'somatic_bedpe': '%s.somatic.bedpe.txt' % out_pon,
            'somatic_highConf_vcf': '%s.somatic.highConf.vcf.gz' % out_pon,
            'somatic_highConf_tbi':
                '%s.somatic.highConf.vcf.gz.tbi' % out_pon,
            'somatic_highConf_bedpe':
                '%s.somatic.highConf.bedpe.txt' % out_pon,
            'somatic_highConf_plot_bedpe':
                '%s.somatic.highconf.plot.bedpe' % out_filter,
            'svFreq_vcf':
                '%s.vcf.gz' % out_svfreq,
            'svFreq_tbi':
                '%s.vcf.gz.tbi' % out_svfreq,
            'annotate_bedpe':
                '%s.annotate.bedpe' % out_svRescue,
            'annotated_raw_vcf':
                '%s.annotated.raw.vcf.gz' % out_svRescue,
            'overlap_bed_txt':
                '%s.overlap.bed.txt.gz' % out_svRescue,
            'overlap_bed_txt_tbi':
                '%s.overlap.bed.txt.gz.tbi' % out_svRescue})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Filter SVs from DELLY calls'
                                       'using Panel of Normal samples'),
                                 add_help=False)


def Delly_sv_PoN_filter_args(parser, subparsers, argv):
    parser.add_argument('--run-id',  dest='run_id',
                        help='Sample id, identifier of the run',
                        required=True)
    parser.add_argument('--raw',  dest='vcfs', nargs='*',
                        help='Input BCF/VCF file(s)',
                        required=True)
    parser.add_argument('--segments', dest='segments',
                        help="Input a BED/txt file", required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output directory', required=True)
    return parser.parse_args(argv)


def Delly_sv_PoN_filter(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = Delly_sv_PoN_filter_args(add_parser(
        subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['tabix']))
    module('add', program_string(profile.programs['samtools_1']))
    module('add', program_string(profile.programs['bcftools']))
    module('add', program_string(profile.programs['vcftools']))
    module('add', program_string(profile.programs['bedtools']))
    module('add', program_string(profile.programs['vcflib']))
    module('add', program_string(profile.programs['Delly_sv_PoN_filter']))

    sv_collection = profile.files['sv_collection']
    genome_fa = profile.files['genome_fa']
    output_merged_prefix = os.path.join(args.out, args.run_id)
    output_merged = '%s.vcf.gz' % output_merged_prefix

    log.log.info('Preparing bcftools concat command line')
    merge_cmd = ['bcftools', 'concat',
                 '--allow-overlaps', '-O', 'v'] + args.vcfs
    bgzip_cmd = ['bgzip', '-c']
    tabix_cmd = ['tabix', '-p', 'vcf', '-f', output_merged]
    vcf2tsv_cmd = ['dellyVcf2Tsv.py', '-v', output_merged,
                   '-o', '%s.bedpe.txt' % output_merged_prefix]
    ziptsv_cmd = ['bgzip', '-f', '%s.bedpe.txt' % output_merged_prefix]
    vcf2tsv_cmd = shlex.split(' '.join(map(str, vcf2tsv_cmd)))
    ziptsv_cmd = shlex.split(' '.join(map(str, ziptsv_cmd)))
    merge_cmd = shlex.split(' '.join(map(str, merge_cmd)))
    bgzip_cmd = shlex.split(' '.join(map(str, bgzip_cmd)))
    tabix_cmd = shlex.split(' '.join(map(str, tabix_cmd)))

    log.log.info('Open file %s to write merged vcf output' % output_merged)
    with open(output_merged, 'wt') as merged_vcf:
        log.log.info('Execute bcftools concat with python subprocess.Popen')
        log.log.info('%s | %s > %s' % (' '.join(map(str, merge_cmd)),
                                       ' '.join(map(str, bgzip_cmd)),
                                       output_merged))
        merge_proc = subprocess.Popen(merge_cmd, stdout=subprocess.PIPE)
        bgzip_proc = subprocess.Popen(bgzip_cmd, stdin=merge_proc.stdout,
                                      stdout=merged_vcf)
        merge_proc.stdout.close()
        out0 = bgzip_proc.communicate()[0]
    log.log.info('Index merged file %s' % output_merged)
    log.log.info(' '.join(map(str, tabix_cmd)))
    tabix_proc = subprocess.Popen(tabix_cmd)
    out1 = tabix_proc.communicate()[0]
    log.log.info('Convert VCF to bedpe format %s' % output_merged)
    log.log.info(' '.join(map(str, vcf2tsv_cmd)))
    bedpe_proc = subprocess.Popen(vcf2tsv_cmd)
    out3 = bedpe_proc.communicate()[0]

    log.log.info('Compress bedpe format %s.bedpe.txt.gz' %
                 output_merged_prefix)
    log.log.info(' '.join(map(str, ziptsv_cmd)))
    ziptsv_proc = subprocess.Popen(ziptsv_cmd)
    out4 = ziptsv_proc.communicate()[0]
    output_bedpe = '%s.bedpe.txt.gz' % output_merged_prefix
    output_noise_filtered_prefix = os.path.join(args.out, args.run_id)
    output_noise_filtered = '%s.noiseFiltered.raw.vcf' % output_noise_filtered_prefix
    output_annotated_prefix = os.path.join(args.out, args.run_id)
    output_annotated = '%s.annotated.raw.vcf.gz' % output_annotated_prefix


    svNoise_cmd = ['sv_noise_filter.py', '-v', output_merged, '-o', output_noise_filtered, '-b', output_bedpe]
    svNoise_cmd = shlex.split(' '.join(map(str, svNoise_cmd)))
    log.log.info('Prepare the noise filter sv command line:')
    log.log.info(' '.join(map(str, svNoise_cmd)))
    svNoise_proc = subprocess.Popen(svNoise_cmd)
    out10 = svNoise_proc.communicate()[0]

    if os.path.exists(output_noise_filtered):
        bgzip_cmd = ['bgzip', '-f', '%s.noiseFiltered.raw.vcf' % output_noise_filtered_prefix]
        tabix_noise_cmd = ['tabix', '-p', 'vcf', '-f', '%s.noiseFiltered.raw.vcf.gz' % output_noise_filtered_prefix]
        bgzip_proc = subprocess.Popen(bgzip_cmd)
        out01 = bgzip_proc.communicate()[0]
        index_svNoise_proc = subprocess.Popen(tabix_noise_cmd)
        out6 = index_svNoise_proc.communicate()[0]

        vcf2tsv_cmd = ['dellyVcf2Tsv.py', '-v', '%s.noiseFiltered.raw.vcf.gz' % output_noise_filtered_prefix,
                       '-o', '%s.noiseFiltered.raw.bedpe.txt' % output_noise_filtered_prefix]
        ziptsv_cmd = ['bgzip', '-f', '%s.noiseFiltered.raw.bedpe.txt' % output_noise_filtered_prefix]
        vcf2tsv_cmd = shlex.split(' '.join(map(str, vcf2tsv_cmd)))
        ziptsv_cmd = shlex.split(' '.join(map(str, ziptsv_cmd)))

        log.log.info('Convert VCF to bedpe format %s' % output_noise_filtered)
        log.log.info(' '.join(map(str, vcf2tsv_cmd)))
        bedpe_proc = subprocess.Popen(vcf2tsv_cmd)
        out7 = bedpe_proc.communicate()[0]

        log.log.info('Compress bedpe format %s.noiseFilterd.bedpe.txt.gz' %
                      output_noise_filtered_prefix)
        log.log.info(' '.join(map(str, ziptsv_cmd)))
        ziptsv_proc = subprocess.Popen(ziptsv_cmd)
        out8 = ziptsv_proc.communicate()[0]
        output_noise_filtered_bedpe = '%s.noiseFiltered.raw.bedpe.txt.gz' % output_noise_filtered_prefix
        svRescue_cmd = ['sv_rescue.py', '-v', '%s.noiseFiltered.raw.vcf.gz' % output_noise_filtered_prefix, '-s', args.segments,
                        '-b', output_noise_filtered_bedpe]
    else:
        svRescue_cmd = ['sv_rescue.py', '-v', output_merged, '-s', args.segments,
                        '-b', output_bedpe]

    bgzip_cmd = ['bgzip', '-c']
    tabix_rescue_cmd = ['tabix', '-p', 'vcf', '-f', output_annotated]

    log.log.info('Prepare the sv rescue command line:')
    with open(output_annotated, 'wt') as rescued_vcf:
        log.log.info('Execute sv rescue with python subprocess.Popen')
        log.log.info('%s | %s > %s' % (' '.join(map(str, svRescue_cmd)),
                                       ' '.join(map(str, bgzip_cmd)),
                                       output_annotated))
        rescue_proc = subprocess.Popen(svRescue_cmd, stdout=subprocess.PIPE)
        bgzip_proc = subprocess.Popen(bgzip_cmd, stdin=rescue_proc.stdout,
                                      stdout=rescued_vcf)
        rescue_proc.stdout.close()
        out0 = bgzip_proc.communicate()[0]
    index_rescue_proc = subprocess.Popen(tabix_rescue_cmd)
    out = index_rescue_proc.communicate()[0]

    filter_cmd = ['annotateMergedSvCalls_finsen_delly.sh', args.run_id,
                  output_annotated, args.out, sv_collection, genome_fa]
    filter_cmd = shlex.split(' '.join(map(str, filter_cmd)))
    log.log.info('Prepare the pcawg filter sv command line:')
    log.log.info(' '.join(map(str, filter_cmd)))
    filter_proc = subprocess.Popen(filter_cmd)
    out5 = filter_proc.communicate()[0]
    code = filter_proc.returncode
    info = 'annotateMergedSvCalls_finsen_delly.sh, exit code: %s' % code
    if code == 0:
        log.log.info(info)
    else:
        log.log.error(info)

    log.log.info('Terminate Delly_sv_PoN_filter')
