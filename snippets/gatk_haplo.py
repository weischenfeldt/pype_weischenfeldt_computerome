import re
import os
import shlex
import subprocess
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 1, 'time': '48:00:00'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    output = '%s' % re.sub('.gz$', '', output)
    output = '%s' % re.sub('.vcf$', '', output)
    return({'vcf': '%s.vcf.gz' % output, 'idx': '%s.vcf.gz.tbi' % output})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(
        module_name, help='Call variant with gatk HaplotypeCaller',
        add_help=False)


def gatk_haplo_args(parser, subparsers, argv):
    parser.add_argument('-i', '--input', dest='input',
                        help='input bam file', required=True)
    parser.add_argument('-m', '--min-mapping-quality', dest='min_map',
                        help=('Exclude alignments from analysis if '
                              'they have a mapping quality less than MIN_MAP'),
                        type=int, default=30)
    parser.add_argument('-q', '--min-base-quality', dest='min_qual',
                        help=('Exclude alleles from analysis if their '
                              'supporting base quality is less than MIN_QUAL'),
                        type=int, default=20)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output vcf file', required=True)
    return parser.parse_args(argv)


def gatk_haplo(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = gatk_haplo_args(add_parser(
        subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['java8']))
    module('add', program_string(profile.programs['gatk']))
    module('add', program_string(profile.programs['tabix']))
    gatk_dir = os.environ['GATK_DIR']
    # gatk_jar = os.path.join(gatk_dir, 'GenomeAnalysisTK.jar')
    # log.log.info('Use GATK jar file at %s' % gatk_jar)

    build = profile.genome_build
    log.log.info('Use genome build %s' % build)

    genome = profile.files['genome_fa']
    log.log.info('Use genome reference %s' % genome)

    output = '%s' % re.sub('\.gz$', '', args.out)
    output = '%s' % re.sub('\.vcf$', '', output)

    output = '%s.vcf.gz' % re.sub('\.vcf$', '', output)

    log.log.info('Preparing gatk command line')
    gatk_cmd = ['gatk', 'HaplotypeCaller', '-R', genome,
                '-mmq', args.min_map, '-mbq', args.min_qual,
                '-I', args.input, '-O', '/dev/stdout']

    bgzip_cmd = ['bgzip', '-c']
    tabix_cmd = ['tabix', '-p', 'vcf', '-f', output]

    gatk_cmd = shlex.split(' '.join(map(str, gatk_cmd)))
    bgzip_cmd = shlex.split(' '.join(map(str, bgzip_cmd)))
    tabix_cmd = shlex.split(' '.join(map(str, tabix_cmd)))

    log.log.info(' '.join(map(str, gatk_cmd)))
    log.log.info('Open file %s in writing mode' % output)
    with open(output, 'wt') as output_file:
        log.log.info('Execute gatk with python subprocess.Popen')
        gatk_proc = subprocess.Popen(gatk_cmd, stdout=subprocess.PIPE)
        bgzip_proc = subprocess.Popen(bgzip_cmd, stdin=gatk_proc.stdout,
                                      stdout=output_file)
        gatk_proc.stdout.close()
        out0 = bgzip_proc.communicate()[0]
    log.log.info('Index compressed VCF %s' % output)
    log.log.info(' '.join(map(str, tabix_cmd)))
    tabix_proc = subprocess.Popen(tabix_cmd)
    out2 = tabix_proc.communicate()[0]

    log.log.info('Terminate gatk')
