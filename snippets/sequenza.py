import os
import shlex
import subprocess
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 4, 'time': '800:00:00', 'mem': '12gb'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    except KeyError:
        output = ''
    output = os.path.abspath(output)
    try:
        sample = argv['--sample']
    except KeyError:
        sample = argv['-s']
    sample_path = os.path.join(output, sample)
    return({'segments': '%s_segments.txt' % sample_path,
            'mutations': '%s_mutations.txt' % sample_path})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Allele specific copy number '
                                       'estimation from tumor genomes'),
                                 add_help=False)


def sequenza_args(parser, subparsers, argv):
    parser.add_argument('-i', '--input', dest='input',
                        help='Input seqz file', required=True)
    parser.add_argument('-s', '--sample', dest='sample',
                        help='Sample name', required=True)
    parser.add_argument('-x', '--x-heterozygous', dest='female',
                        help=('Flag to set when the X chromomeme '
                              'is heterozygous. eg: set it for '
                              'female genomes'), action='store_true')
    parser.add_argument('-o', '--out', dest='out',
                        help='Output path')
    parser.add_argument('-n', '--ncpu', dest='ncpu', type=int,
                        help='Number of CPU, defalt 4', default=4)
    return parser.parse_args(argv)


def sequenza(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = sequenza_args(add_parser(
        subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['intelperflibs']))
    module('add', program_string(profile.programs['gcc']))    
    module('add', program_string(profile.programs['R361']))   

    log.log.info('Prepare R code')
    if args.female:
        female = 'TRUE'
    else:
        female = 'FALSE'
    log.log.info('Read -x argument, set sequenza ergument "female=%s"' % female)

    r_code = '''
    library(sequenza)
    extract <- sequenza.extract("%(input_seqz)s")
    CP <- sequenza.fit(extract, female = %(female)s,
                       mc.cores = %(mc_cores)i)
    sequenza.results(sequenza.extract = extract, cp.table = CP,
                     sample.id = "%(sample_name)s",
                     female = %(female)s,
                     out.dir="%(output_path)s")
    ''' % {'input_seqz': args.input,
           'sample_name': args.sample,
           'female': female,
           'mc_cores': args.ncpu,
           'output_path': args.out}

    log.log.info('Prepare R command line')
    R_cmd = ['R', '--vanilla', '--slave']

    R_cmd = shlex.split(' '.join(map(str, R_cmd)))

    log.log.info(' '.join(map(str, R_cmd)))
    log.log.info('Execute R with python subprocess.Popen')
    sequenza_proc = subprocess.Popen(R_cmd, stdin=subprocess.PIPE)
    log.log.info('Pipe R code into R command line: %s'
                 % r_code)
    sequenza_proc.stdin.write(r_code)
    sequenza_proc.stdin.close()
    out0 = sequenza_proc.communicate()[0]

    log.log.info('Terminate sequenza')
