
import os
import shlex
import subprocess
import Queue
from threading import Thread
from pype.misc import generate_uid, readFastqTag
from pype.env_modules import get_module_cmd, program_string

#create work queue
q = Queue.Queue()

class RseqcTask:
    def __init__(self, command, args, resultFile=None, logHandle=None):
        self.cmd = command
        self.args = args
        self.resultFile = resultFile
        self.logHandle = logHandle

def requirements():
    return({'ncpu': 8, 'time': '4:00:00'})


def results(argv):
    try:
        outdir = argv['--outputdir']
    except KeyError:
        outdir = argv['-o']
    finishFile = os.path.join(outdir,"rseqc","rseqc.finished")
    return {'rseqcfinished':finishFile}


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Perform quality control of RNA Seq data with rseqc'),
                                 add_help=False)

def rseqc_args(parser, subparsers, argv):
    parser.add_argument('--samplename', '-n', dest='samplename',
                        help=('The name of the sample'),
                        required=True)
    parser.add_argument('--input', '-i', dest='inputbam',
                        help=('The input bam file to analyse'),
                        required=True)
    parser.add_argument('--outputdir','-o', dest='out',
                        help='The path to use for output files',
                        required=True)
    parser.add_argument('--SEPE', '-s', dest='SEPE',
                        help='Data is single end (SE) or paired end (PE)',
                        required=True)
    parser.add_argument('--refmodel', '-r', dest='refmodel',
                        help='A bed file containing the transcript/exon model data. Defaults to rseqc_model_bed from profile')
    parser.add_argument('--refmodelHK', '-h', dest='refmodelHK',
                        help='A bed file containing a housekeeping subset of the transcript/exon model data. Defaults to <rseqc_model_bed>.HouseKeeping')
    parser.add_argument('--threads', dest='threads',
                        help='Specifies the number of files which can be processed simultaneously',
                        required=False, default=1)
    return parser.parse_args(argv)


def rseqc(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = rseqc_args(add_parser(subparsers, module_name), subparsers, argv)

    #Load Moduels
    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['intelcompiler']))
    module('add', program_string(profile.programs['R340']))
    module('add', program_string(profile.programs['rseqc']))



    #Create Output directory
    outPath = os.path.join(args.out,"rseqc")
    finishFile = os.path.join(outPath,"rseqc.finished")
    if not os.path.exists(outPath):
        os.makedirs(outPath)
        log.log.info('Created outdir at: %s' % outPath)

    #Output Prefix
    outFileNamePrefix = os.path.join(outPath,args.samplename)

    #Set transcript model file
    refModel = ''
    if args.refmodel is None:
        refModel = profile.files['rseqc_model_bed']
    else:
        refModel = args.refmodel

    if args.refmodelHK is None:
        refModelHK = refModel + '.HouseKeeping'
    else:
        refModelHK = args.refmodelHK
    
    #Spool up work handler threads
    for i in range(int(args.threads)):
        t = Thread(target=worker)
        t.daemon = True
        t.start()

    #Add rseqc components to queue
    q.put(RseqcTask('bam_stat.py', ['-i', args.inputbam], logHandle=log.log))
    q.put(RseqcTask('clipping_profile.py', ['-i', args.inputbam, '-s', args.SEPE, '-o', outFileNamePrefix], logHandle=log.log))
    q.put(RseqcTask('geneBody_coverage.py', ['-i', args.inputbam, '-r', refModelHK, '-o', outFileNamePrefix, '-f', 'png'], logHandle=log.log))
    q.put(RseqcTask('inner_distance.py', ['-i', args.inputbam, '-r', refModel, '-o', outFileNamePrefix], logHandle=log.log))
    q.put(RseqcTask('junction_annotation.py', ['-i', args.inputbam, '-r', refModel, '-o', outFileNamePrefix], logHandle=log.log))
    q.put(RseqcTask('junction_saturation.py', ['-i', args.inputbam, '-r', refModel, '-o', outFileNamePrefix], logHandle=log.log))
    q.put(RseqcTask('read_distribution.py', ['-i', args.inputbam, '-r', refModel], resultFile=outFileNamePrefix+'.read_distribution.txt', logHandle=log.log))
    q.put(RseqcTask('read_duplication.py', ['-i', args.inputbam, '-o', outFileNamePrefix], logHandle=log.log))
    q.put(RseqcTask('read_quality.py', ['-i', args.inputbam, '-o', outFileNamePrefix], logHandle=log.log))
    q.put(RseqcTask('read_GC.py', ['-i', args.inputbam, '-o', outFileNamePrefix], logHandle=log.log))

    #Wait for queue to complete
    q.join()

    touch_process = subprocess.Popen(["touch", finishFile])
    log.log.info('Completed rseqc execution')


def worker():
    while True:
        item = q.get()
        do_work(item)
        q.task_done()

def do_work(task):

    cmdString = task.cmd + ' ' + ' '.join(task.args)

    task.logHandle.info('Execute rseqc task with python subprocess.Popen. CMD: %s' %  (cmdString))

    rseqc_cmd = shlex.split(cmdString)

    rseqc_proc = subprocess.Popen(rseqc_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out0, Err0 = rseqc_proc.communicate()

    if (task.resultFile is None): ##Output written to file by rseqc
        task.logHandle.info('______________________________________________________________________________' )
        task.logHandle.info('%s had the following output: %s' % (task.cmd, out0))
        task.logHandle.info('______________________________________________________________________________' )
    else: #resqc script output is printed to stdout
        try:
            rf = open(task.resultFile, 'w')
            rf.write(out0)
        except:
             task.logHandle.error('Unable to open %s for writing' % task.resultFile)

    task.logHandle.info(Err0)
    task.logHandle.info('Completed rseqc task: %s' %  task.cmd)


