import os
import re
import shlex
import subprocess
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 1, 'time': '12:00:00'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    output = re.sub('.bcf$', '', output)
    output = re.sub('.gz$', '', output)
    output = re.sub('.vcf$', '', output)
    return({'vcf': '%s.vcf.gz' % output, 'tbi': '%s.vcf.gz.tbi' % output})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Combine indexed BCF/VCF files'),
                                 add_help=False)


def vcf_concat_args(parser, subparsers, argv):
    parser.add_argument('-i', '--input', dest='vcfs', nargs='*',
                        help='List of VCF/BCF files to merge',
                        required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output VCF file', required=True)
    return parser.parse_args(argv)


def vcf_concat(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = vcf_concat_args(add_parser(
        subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['tabix']))
    module('add', program_string(profile.programs['bcftools']))

    output = re.sub('.bcf$', '', args.out)
    output = re.sub('.gz$', '', output)
    output = re.sub('.vcf$', '', output)
    output_merged = '%s.vcf.gz' % output

    output_dir, output_file = os.path.split(os.path.abspath(output))
    if not os.path.exists(output_dir):
        log.log.info('Create output directory %s' % output_dir)
        os.makedirs(output_dir)

    log.log.info('Preparing bcftools concat command line')
    merge_cmd = ['bcftools', 'concat',
                 '--allow-overlaps', '-O', 'v'] + args.vcfs
    bgzip_cmd = ['bgzip', '-c']
    tabix_cmd = ['tabix', '-p', 'vcf', '-f', output_merged]

    merge_cmd = shlex.split(' '.join(map(str, merge_cmd)))
    bgzip_cmd = shlex.split(' '.join(map(str, bgzip_cmd)))
    tabix_cmd = shlex.split(' '.join(map(str, tabix_cmd)))

    log.log.info('Open file %s to write merged vcf output' % output_merged)
    with open(output_merged, 'wt') as merged_vcf:
        log.log.info('Execute bcftools concat with python subprocess.Popen')
        log.log.info('%s | %s > %s' % (' '.join(map(str, merge_cmd)),
                                       ' '.join(map(str, bgzip_cmd)),
                                       output_merged))
        merge_proc = subprocess.Popen(merge_cmd, stdout=subprocess.PIPE)
        bgzip_proc = subprocess.Popen(bgzip_cmd, stdin=merge_proc.stdout,
                                      stdout=merged_vcf)
        merge_proc.stdout.close()
        out0 = bgzip_proc.communicate()[0]

    log.log.info('Index merged file %s' % output_merged)
    log.log.info(' '.join(map(str, tabix_cmd)))
    tabix_proc = subprocess.Popen(tabix_cmd)
    out2 = tabix_proc.communicate()[0]

    log.log.info('Terminate vcf_concat')
