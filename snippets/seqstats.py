import os
import shlex
import subprocess

from pype.misc import generate_uid
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 1, 'time': '12:00:00'})


def results(argv):
    try:
        file = argv['--bam']
    except KeyError:
        file = argv['-b']
    try:
        path = argv['--out']
    except KeyError:
        path = argv['-o']

    sample_dir, sample = os.path.split(file)
    sample = os.path.splitext(sample)[0]
    output = os.path.join(path, sample, sample)

    return({'pdf': '%s.pdf' % output, 'stats': '%s.stats' % output})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name, help='Perform statistics and generate QC plot for bam files', add_help=False)


def seqstats_args(parser, subparsers, argv):
    parser.add_argument('-b', '--bam', dest='bam', type=str,
                        help='BAM file', required=True)
    parser.add_argument('-a', '--aligner', dest='align', type=str,
                        help='Aligner type', action='store', choices=['bwa', 'eland', 'novoalign'], default='bwa')
    parser.add_argument('-o', '--out', dest='out', type=str,
                        help='Output folder', required=True)
    parser.add_argument('-t', '--tmp', dest='tmp',
                        help='Temporary folder',
                        default='/scratch')
    return parser.parse_args(argv)


def seqstats(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = seqstats_args(add_parser(subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools', 'ngs')
    module('add', program_string(profile.programs['samtools_1']))
    module('add', program_string(profile.programs['intelperflibs']))
    module('add', program_string(profile.programs['gcc']))    
    module('add', program_string(profile.programs['R361']))   
    module('add', program_string(profile.programs['cov_tool']))
    module('add', program_string(profile.programs['seqstats']))

    genome = profile.files['genome_fa']
    gc = profile.files['gc_10000_bed']

    sample_dir, sample = os.path.split(args.bam)
    sample = os.path.splitext(sample)[0]

    output = os.path.join(args.out, sample)

    if not os.path.exists(output):
        os.makedirs(output)
    output = os.path.join(output, sample)

    log.log.info('Preparing seqstats  command line')
    random_str = generate_uid()
    seqstats_cmd = ['run_seqstats', '--bam', args.bam, '--out', output, '--gc_file', gc,
                    '--fasta', genome, '--tmp', os.path.join(args.tmp, 'seqstats_%s' % random_str)]
    log.log.info(' '.join(map(str, seqstats_cmd)))
    seqstats_cmd = shlex.split(' '.join(map(str, seqstats_cmd)))
    log.log.info('Execute run_seqstats with python subprocess.Popen')
    with open(os.devnull, 'w') as dev_null:
        seqstats_proc = subprocess.Popen(seqstats_cmd, stderr=dev_null)
        seqstats_proc.communicate()
    log.log.info('Terminate seqstats')
