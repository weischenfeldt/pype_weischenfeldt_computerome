import os
import shlex
import subprocess
from pype.misc import generate_uid
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 2, 'time': '10:00:00'})


def results(argv):
    resultDict = dict()

    outdir = argv['--out']

    fq1 = argv['--fq1']
    fq1ResultPrefix = os.path.join(outdir,"fastqc", os.path.basename(fq1).replace(".fastq.gz",""))
    resultDict["fq1HtmlReport"]=  "%s_fastqc.html" % fq1ResultPrefix
    resultDict["fq1ZipReport"]=  "%s_fastqc.zip" % fq1ResultPrefix

    if '--fq2' in argv:
        fq2 = argv['--fq2']
        fq2ResultPrefix = os.path.join(outdir,"fastqc", os.path.basename(fq2).replace(".fastq.gz",""))
        resultDict["fq2HtmlReport"]=  "%s_fastqc.html" % fq2ResultPrefix
        resultDict["fq2ZipReport"]=  "%s_fastqc.zip" % fq2ResultPrefix

    return resultDict


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Perform QC stats with fastQC'),
                                 add_help=False)


def fastqc_args(parser, subparsers, argv):
    parser.add_argument('--fq1', dest='f1',
                        help='First fastQ file',
                        required=True)
    parser.add_argument('--fq2', dest='f2',
                        help=('Second fastQ file. Not required if single end'),
                        required=False)
    parser.add_argument('--out', dest='out',
                        help='Output directory',
                        default=None, required=True)
    parser.add_argument('--tmp', dest='tmp',
                        help='Temporary folder',
                        default='/scratch')
    parser.add_argument('--threads', dest='threads',
                        help='Number of files which can be processed simultaneously',
                        default="2")                        
    return parser.parse_args(argv)


def fastqc(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = fastqc_args(add_parser(subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['perl']))
    module('add', program_string(profile.programs['java7']))
    module('add', program_string(profile.programs['fastqc']))

    outDir = os.path.join(args.out, "fastqc")
    finishFile = os.path.join(outDir,"fastqc.finished")
    if not os.path.exists(outDir):
        os.makedirs(outDir)
        log.log.info('Created outdir at: %s' % outDir) 

    fastqc_cmd = ['fastqc', '--outdir', outDir, '--threads', args.threads, '--dir', args.tmp, args.f1 ]
    if args.f2:
        fastqc_cmd.append(args.f2)

    log.log.info('Execute fastqc with python subprocess.Popen. CMD: %s' % ' '.join(map(str, fastqc_cmd)))
    fastqc_cmd = shlex.split(' '.join(map(str, fastqc_cmd)))
    fastqc_proc = subprocess.Popen(fastqc_cmd, stdout=subprocess.PIPE)
    out0 = fastqc_proc.communicate()[0]
    log.log.info('Standard Out:')
    log.log.info(out0)
    log.log.info('Completed FastQC execution')