import os
import shlex
import subprocess
from pype.misc import generate_uid, readFastqTag
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 8, 'time': '8:00:00','mem':'48gb'})


def results(argv):
    try:
        outdir = argv['--out']
    except KeyError:
        outdir = argv['-o']

    sample = argv['--samplename']

    alignBam = os.path.join(outdir,sample+"_Aligned.sortedByCoord.out.bam")
    chimeraBam = os.path.join(outdir,sample+"_Chimeric.out.bam")
    return {'alignBam':alignBam, 'chimeraBam': chimeraBam}


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Align RNA Seq Reads with STAR'),
                                 add_help=False)


def star_args(parser, subparsers, argv):
    parser.add_argument('--samplename', dest='samplename',
                        help=('Sample name used to prefix output files'),
                        required=False)
    parser.add_argument('--genomeDir', dest='genomeDir',
                        help=('Location of the STAR pre-processed genome files. Uses star_GRCh37_v19 from pipeline profile by default'),
                        required=False)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output directory',
                        default=None, required=True)
    parser.add_argument('-t', '--tmp', dest='tmp',
                        help='Temporary folder',
                        default='/home/projects/cu_10027/scratch/')
    parser.add_argument('-j', '--threads', dest='threads',
                        help='Number of parallel threads',
                        default="8")
    parser.add_argument('--chimSegMin', dest='chimSegMin',
                        help='The minimum length sequence present to call a chimeric read',
                        default="25")                                                    
    parser.add_argument('--bamrg', dest='bamrg',
                        help='The read group line for the bam file. Read from FastQ by default',
                        required=False)                        
    parser.add_argument('--fq1', dest='fq1', 
                        help='Read 1 fastq file',
                        required=True)
    parser.add_argument('--fq2', dest='fq2', 
                        help='Read 2 fastq file',
                        required=True)                        
    return parser.parse_args(argv)


def star(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = star_args(add_parser(subparsers, module_name), subparsers, argv)

    #Load Moduels
    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['samtools_1']))
    module('add', program_string(profile.programs['star']))

    #Create Temp folder
    tempFolder = os.path.join(args.tmp,"STAR_"+args.samplename)
    if os.path.exists(tempFolder):
        try:
            os.rmdir(tempFolder)
        except:
            print("Died trying to delete already existing STAR temp dir (which will kill STAR). Directory must not be empty. Manually remove %s and retry" % tempFolder)
            exit(1010)
    
    #Output Prefix
    outFileNamePrefix = os.path.join(args.out,args.samplename+"_")
    
    #Set Genome Directory
    genomeDir = ''
    if args.genomeDir is None:
        genomeDir = profile.files['star_GRCh37_v19']
    else:
        genomeDir = args.genomeDir

    #Fetch RG header from FastQ Files
    rgHeaders = []
    fqHeader = readFastqTag(args.fq1)
    if "Machine" in fqHeader:
        RGID='.'.join([fqHeader["Flowcell"],fqHeader["Lane"],fqHeader["Index"]])
        rgHeader="ID:%s\tPL:Illumina\tDS:%s\tSM:%s" % (RGID, RGID, args.samplename)
        if rgHeader not in rgHeaders:
            rgHeaders.append(rgHeader)
    else:
        log.log.error('Failed to read tag from Fastq')
    outSAMattrRGline= " , ".join(rgHeaders)

    #Create Output directory
    if not os.path.exists(args.out):
        os.makedirs(args.out)
        log.log.info('Created outdir at: %s' % args.out) 

    #Compile command
    star_cmd = ['STAR', 
                "--outSAMtype", "BAM", "SortedByCoordinate", 
                "--readFilesCommand", "zcat",
                '--outFileNamePrefix', outFileNamePrefix, 
                '--runThreadN', args.threads, 
                '--outTmpDir', tempFolder,
                '--genomeDir', genomeDir, 
                '--outSAMattrRGline', outSAMattrRGline,
                '--chimSegmentMin', args.chimSegMin,
                '--quantMode', 'GeneCounts',
                '--readFilesIn', args.fq1, args.fq2] 

    log.log.info('Execute star with python subprocess.Popen. CMD: %s' % ' '.join(map(str, star_cmd)))
    star_cmd = shlex.split(' '.join(map(str, star_cmd)))
    star_proc = subprocess.Popen(star_cmd, stdout=subprocess.PIPE)
    out0 = star_proc.communicate()[0]
    log.log.info('STAR said:')
    log.log.info(out0)

    bamFile = os.path.join(outFileNamePrefix+"Aligned.sortedByCoord.out.bam")
    samtools_cmd = shlex.split(' '.join(map(str, ["samtools","index", bamFile])))
    samtools_proc = subprocess.Popen(samtools_cmd, stdout=subprocess.PIPE)
    out0 = samtools_proc.communicate()[0]
    log.log.info('Samtools Index said:')
    log.log.info(out0)

    log.log.info('Completed star execution')