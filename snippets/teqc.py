
import os
import shlex
import subprocess
from pype.misc import generate_uid
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 1, 'time': '01:00:00'})


def results(argv):
    return None

def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Generate exome capture QC report with TEQC'),
                                 add_help=False)

                                 
def teqc_args(parser, subparsers, argv):
    parser.add_argument('-s', dest='samplename',
                        help=('Sample name'),
                        required=True)
    parser.add_argument('-b', dest='bamfile',
                        help=('Input BAM file'),
                        required=True) 
    parser.add_argument('-o', dest='out',
                        help=('Output directory'),
                        required=True)                                
    parser.add_argument('-p', dest='pairedend',
                        help=('Reads are paired end (T/F)'),
                        required=True)                                
    parser.add_argument('-t', dest='targetsfile',
                        help='Exome capture targets BED file',
                        required=True)
    parser.add_argument('-n', dest='targetsname',
                        help='Exome capture assay name',
                        required=True)
    parser.add_argument('-g', dest='genome',
                        help='Genome of targets BED and BAM (accepts hg18/19, else set -z genomesize and -r referencename)',
                        required=False)
    parser.add_argument('-z', dest='genomesize',
                        help='size of genome (set for genomes other than hg18/19)',
                        required=False)
    parser.add_argument('-r', dest='referencename',
                        help='Name of reference genome (set for genomes other than hg18/19)',
                        required=False)
    return parser.parse_args(argv)


def teqc(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = teqc_args(add_parser(subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['teqc']))
    module('add', program_string(profile.programs['R3']))

    log.log.info("Input capture target BED file is: %s" % args.targetsfile)
    log.log.info("Input BAM is: %s" % args.bamfile)
    log.log.info("TEQC output directory set to: %s" % args.out)

    if args.genome:
        log.log.info("Using supplied genome: %s" % args.genome)
    elif args.genomesize and args.referencename:
        log.log.info("Genome not supplied, using supplied genome size: %s and reference name: %s" % (args.genomesize, args.referencename))
    else:
        log.log.error('Genome or genome size and reference name were not supplied')
        exit(1)    

    if args.pairedend == 'T':
        pairedEnd='T'
        log.log.info('Treating data as paired end')
    else:
        pairedEnd='F'
        log.log.info('Treating data as single end')
    
    teqc_cmd = ['teqc']    
    teqc_cmd.append("--bamfile=%s" % args.bamfile)
    teqc_cmd.append("--targetsfile=%s" % args.targetsfile)
    teqc_cmd.append("--samplename=%s" % args.samplename)
    teqc_cmd.append("--targetsname=%s" % args.targetsname)
    teqc_cmd.append("--destdir=%s" % args.out)
    teqc_cmd.append("--pairedend=%s" % args.pairedend)    
    if args.genome is not None: 
        teqc_cmd.append("--genome=%s" % args.genome)
    if args.genomesize is not None: 
        teqc_cmd.append("--genomesize=%s" % args.genomesize)
    if args.referencename is not None: 
        teqc_cmd.append("--referencename=%s" % args.referencename)   
    log.log.info('Exec Cmd is: %s' %  " ".join(teqc_cmd))
    log.log.info('Execute TEQC via Rscript with python subprocess.Popen')
    teqc_proc = subprocess.Popen(teqc_cmd, stdout=subprocess.PIPE)
    out0 = teqc_proc.communicate()[0]
    log.log.info(out0)
    log.log.info('Finished TEQC')
