import re
import os
import shlex
import subprocess
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 1, 'time': '48:00:00'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    return({'table': output})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(
        module_name, help='Tabulates pileup metrics for inferring contamination',
        add_help=False)


def gatk_pileup_summary_args(parser, subparsers, argv):
    parser.add_argument('-i', '--input', dest='input',
                        help='input bam file', required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output table file', required=True)
    parser.add_argument('-l', '--intervals', dest='intervals',
                        help='One or more genomic intervals over which to operate')
    return parser.parse_args(argv)


def gatk_pileup_summary(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = gatk_pileup_summary_args(add_parser(
        subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['java8']))
    module('add', program_string(profile.programs['gatk']))
    module('add', program_string(profile.programs['tabix']))
    gatk_dir = os.environ['GATK_DIR']
    # gatk_jar = os.path.join(gatk_dir, 'GenomeAnalysisTK.jar')
    # log.log.info('Use GATK jar file at %s' % gatk_jar)

    build = profile.genome_build
    log.log.info('Use genome build %s' % build)

    snp = profile.files['ExAC']
    log.log.info('Use snp collection at %s' % snp)

    output = args.out

    log.log.info('Preparing gatk command line')
    gatk_cmd = ['gatk', 'GetPileupSummaries', '-V', snp,
                '-I', args.input, '-O', output]

    if args.intervals:
        gatk_cmd += ['-L', args.intervals]

    gatk_cmd = shlex.split(' '.join(map(str, gatk_cmd)))

    log.log.info(' '.join(map(str, gatk_cmd)))
    log.log.info('Execute gatk with python subprocess.Popen')
    gatk_proc = subprocess.Popen(gatk_cmd)
    out2 = gatk_proc.communicate()[0]

    log.log.info('Terminate gatk')
