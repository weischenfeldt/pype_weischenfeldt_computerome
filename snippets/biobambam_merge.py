import os
import shlex
import subprocess

from pype.misc import generate_uid
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 1, 'time': '12:00:00'})


def results(argv):
    try:
        file = argv['--out']
    except KeyError:
        file = argv['-o']
    return({'bam': file, 'bai': '%s.bai' % file})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Mark duplicates and merge BAMs '
                                       'with biobambam'),
                                 add_help=False)


def biobambam_merge_args(parser, subparsers, argv):
    parser.add_argument('-b', '--bam', dest='bams', nargs='*',
                        help='List of BAM files to merge', required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output name for the merged BAM file',
                        required=True)
    parser.add_argument('-t', '--tmp', dest='tmp',
                        help='Temporary folder',
                        default='/scratch')
    return parser.parse_args(argv)


def biobambam_merge(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = biobambam_merge_args(add_parser(subparsers, module_name),
                                subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools', 'ngs')
    module('add', program_string(profile.programs['biobambam2']))

    log.log.info('Preparing biobambam2 bammarkduplicates2  command line')
    random_str = generate_uid()
    rmdpmerge_cmd = ['bammarkduplicates2', 'O=%s' % args.out, 'index=1',
                     'indexfilename=%s.bai' % args.out,
                     'tmpfile=%s' % os.path.join(
                         args.tmp, 'bammarkduplicates2_%s' % random_str)]
    for input in args.bams:
        rmdpmerge_cmd.append('I=%s' % input)
    log.log.info(' '.join(map(str, rmdpmerge_cmd)))
    rmdpmerge_cmd = shlex.split(' '.join(map(str, rmdpmerge_cmd)))
    log.log.info('Execute bam bammarkduplicates2 with python subprocess.Popen')
    rmdpmerge_proc = subprocess.Popen(rmdpmerge_cmd, stdout=subprocess.PIPE)
    out0 = rmdpmerge_proc.communicate()[0]
    log.log.info('Terminate bammarkduplicates2')
