import os
import re
import shlex
import subprocess
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 2, 'time': '800:00:00', 'mem': '1gb'})


def results(argv):
    try:
        chr = argv['--chromosome']
    except KeyError:
        chr = argv['-c']
    except KeyError:
        chr = None
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    output = re.sub('.gz$', '', output)
    output = re.sub('.seqz$', '', output)
    output = re.sub('.sqz$', '', output)
    if chr is not None:
        output_dir, output_file = os.path.split(os.path.abspath(output))
        output_file = '%s_part_%s' % (output_file, chr)
        output = os.path.join(output_dir, output_file)
    return({'seqz': '%s.seqz.gz' % output,
            'idx': '%s.seqz.gz.tbi' % output})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Parse normal/tumor BAM pairs '
                                       'to create a seqz file'),
                                 add_help=False)


def bam2seqz_args(parser, subparsers, argv):
    parser.add_argument('-n', '--normal', dest='normal',
                        help='Normal sorted and indexed BAM file',
                        required=True)
    parser.add_argument('-t', '--tumor', dest='tumor',
                        help='Tumor sorted and indexed BAM file',
                        required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output seqz file', required=True)
    parser.add_argument('-c', '--chromosome', dest='chromosome',
                        help='Select a specific chromosome or coordinate')
    return parser.parse_args(argv)


def bam2seqz(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = bam2seqz_args(add_parser(
        subparsers, module_name), subparsers, argv)
    log.log.info('Load env module modules')

    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['tabix']))
    module('add', program_string(profile.programs['samtools_1']))
    module('add', program_string(profile.programs['sequenza-utils']))

    build = profile.genome_build
    log.log.info('Use genome build %s' % build)

    genome = profile.files['genome_fa']
    log.log.info('Use genome reference %s' % genome)
    gc_wig = profile.files['genome_gc_wig']
    log.log.info('Use genome GC wiggle file %s' % gc_wig)

    output = re.sub('.gz$', '', args.out)
    output = re.sub('.seqz$', '', output)
    output = re.sub('.sqz$', '', output)

    output_dir, output_file = os.path.split(os.path.abspath(output))

    log.log.info('Preparing bam2seqz command line')
    seqz_cmd = ['sequenza-utils', 'bam2seqz', '-t', args.tumor,
                '-n', args.normal, '-F', genome, '-gc', gc_wig]
    if args.chromosome is not None:
        output_file = '%s_part_%s' % (output_file, args.chromosome)
        output = os.path.join(output_dir, output_file)
        prefix = chromosome_prefix(args.chromosome, '%s.fai' % genome, log)
        if prefix is 'chr':
            chromosome = 'chr%s' % args.chromosome
        elif prefix is 'stripchr':
            chromosome = args.chromosome[:3]
        else:
            chromosome = args.chromosome
        seqz_cmd += ['-C', chromosome]

    seqz_cmd += ['-o', '%s.seqz.gz' % output]

    seqz_cmd = shlex.split(' '.join(map(str, seqz_cmd)))

    log.log.info(' '.join(map(str, seqz_cmd)))
    log.log.info('Execute sequenza-utils with python subprocess.Popen')
    sequenza_proc = subprocess.Popen(seqz_cmd)
    out0 = sequenza_proc.communicate()[0]

    log.log.info('Terminate bam2seqz')


def chromosome_prefix(chromosome, faidx_file, log):
    prefix = None
    chromosome = str(chromosome)
    with open(faidx_file, 'rt') as faidx:
        for line in faidx:
            if line.startswith(chromosome):
                prefix = ''
            elif line.startswith('chr%s' % chromosome):
                prefix = 'chr'
            else:
                if chromosome.startswith('chr'):
                    if line.startswith(chromosome[3:]):
                        prefix = 'stripchr'
    if prefix is not None:
        return prefix
    else:
        log.log.warning(('Chromosome %s is not present '
                         'in the fasta reference' % chromosome))
        return chromosome
